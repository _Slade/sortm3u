#!/usr/bin/perl
use warnings;
use strict;
use List::Util qw(pairs);
use List::UtilsBy qw(sort_by uniq_by);
print @$_ for 
    uniq_by { $_->[0] } 
    sort_by { $_->[0] }
        pairs <ARGV>;
