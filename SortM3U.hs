{-# LANGUAGE OverloadedStrings #-}
module SortM3U where
import qualified Data.List as L
import qualified Data.Text as T
import qualified System.IO as IO
import Data.Text (Text)
import Prelude hiding (lines)
import System.Environment (getArgs)
import System.IO (openFile, stdin)

-- An entry in these specific m3u files has one line beginning with
-- a `#' that has the title, followed by a second line with the URI.
type Entry = (Text, Text)

main :: IO ()
main = do
    args <- getArgs
    -- If we got `-n', we want to sort on the first number in the line.
    let sortFunc = if "-n" `L.elem` args
        then L.sortOn (\(a, _) -> extractInt a)
        else L.sortOn (\(a, _) -> a)

    files <- return $ filter (/= "-n") args
    fh <- if not . null $ files
        then openFile (head $ dropWhile (== "-n") args) IO.ReadMode
        else return stdin

    lines <- IO.hGetContents fh
    mapM_ putPairLn (L.nub . sortFunc . toPairs . T.lines $ T.pack lines)
    IO.hClose fh

putPairLn :: Entry -> IO ()
putPairLn (a, b) = putT a >> putT b
    where putT = putStrLn . T.unpack

toPairs :: [a] -> [(a, a)]
toPairs []         = []
toPairs (_:[])     = error "Uneven list"
toPairs (x0:x1:xs) = (x0, x1) : toPairs xs

extractInt :: Text -> Maybe Int
extractInt str = strToInt
    . T.unpack
    . T.takeWhile isDigit
    $ T.dropWhile (not . isDigit) str
    where
        strToInt :: String -> Maybe Int
        strToInt []  = Nothing
        strToInt s   = Just $ read s
isDigit :: Char -> Bool
isDigit c = '0' <= c && c <= '9'
