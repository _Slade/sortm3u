NAME = SortM3U
JUNK = $(addprefix $(NAME), .hi .o .prof)

SortM3U : $(NAME).hs
	ghc -Wall -main-is $(NAME).main $(NAME).hs

clean :
	rm -fv $(JUNK)
